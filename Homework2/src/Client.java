
public class Client {
	private int stayTime;
	private int id;
	public Client(int stayTime){
		this.stayTime=stayTime;
	}
	public void setStayTime(int stayTime){
		this.stayTime=stayTime;
	}
	public int getStayTime(){
		return this.stayTime;
	}
	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return this.id;
	}
}
