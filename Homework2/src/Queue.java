import java.util.*;

public class Queue implements Runnable{
	private final int nr;
	private int wt;
	private int pt;
	private static String text="";
	private List<Client> clients;
	private int cliNr;
	public Queue(int nr){
		this.nr=nr;
		clients=new ArrayList<Client>();
		cliNr=0;
		pt=0;
		wt=0;
	}
	public void run(){
		Client auxCli;
		while(this.clients.size()>0){
		auxCli=clients.get(0);
		try{
			Thread.sleep(auxCli.getStayTime());
			//adding the text corresponding to the order in which the clients were served to text which is a static instance variable
			text=text + ("SERVED:Client nr. " + clients.get(0).getId() + " for queue nr. " + this.nr + " was served.\n");
			clients.remove(0); //removing clients from queue after serving
			this.pt=this.pt+auxCli.getStayTime();//computing the total time that passed during servings
			this.wt=this.wt-auxCli.getStayTime();//updating the waiting time of the queue
			
		}catch(Exception e){
			
		}
		}
	}
	//adds a client to the list
	public void addClient(Client c){
		
		this.cliNr++;
		c.setId(cliNr);
		this.wt=this.wt+c.getStayTime();
		this.clients.add(c);
	}
	
	public int getClientNr(){
		return this.cliNr;
	}
	public int getPassingTime(){
		return pt;
	}
	public int getWaitingTime(){
		return wt;
	}
	public int getQueueNr(){
		return this.nr;
	}
	public List<Client> getQueue(){
		return this.clients;
	}
	public static String getText(){
		return text;
	}
	
}
