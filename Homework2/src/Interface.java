import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.util.*
;public class Interface extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interface() {
		setTitle("Queue Simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 547, 386);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(133, 31, 264, 191);
		contentPane.add(scrollPane);
		
		JTextArea txtrJnnj = new JTextArea();
		scrollPane.setViewportView(txtrJnnj);
		txtrJnnj.setEditable(false);
		
		JLabel lblLog = new JLabel("    LOG");
		lblLog.setBounds(232, 11, 46, 14);
		contentPane.add(lblLog);
		
		JLabel lblOraDeschidere = new JLabel("ORA DESCHIDERE");
		lblOraDeschidere.setBounds(10, 32, 104, 14);
		contentPane.add(lblOraDeschidere);
		
		textField = new JTextField();
		textField.setBounds(10, 44, 113, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblOraInchidere = new JLabel("ORA INCHIDERE");
		lblOraInchidere.setBounds(10, 75, 96, 14);
		contentPane.add(lblOraInchidere);
		
		textField_1 = new JTextField();
		textField_1.setBounds(10, 89, 113, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblIntervalTimpServire = new JLabel("      INTERVAL TIMP SERVIRE(ms)");
		lblIntervalTimpServire.setBounds(168, 233, 213, 14);
		contentPane.add(lblIntervalTimpServire);
		
		textField_2 = new JTextField();
		textField_2.setBounds(168, 249, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(264, 249, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel label = new JLabel("-");
		label.setBounds(256, 251, 22, 17);
		contentPane.add(label);
		
		JLabel lblIntervatTimpIntre = new JLabel("INTERVAL TIMP DE VENIRE INTRE CLIENTI(ms)");
		lblIntervatTimpIntre.setBounds(143, 280, 278, 14);
		contentPane.add(lblIntervatTimpIntre);
		
		textField_4 = new JTextField();
		textField_4.setBounds(168, 295, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel label_1 = new JLabel("-");
		label_1.setBounds(256, 298, 46, 14);
		contentPane.add(label_1);
		
		textField_5 = new JTextField();
		textField_5.setBounds(264, 295, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNrClienti = new JLabel("NR. COZI");
		lblNrClienti.setBounds(427, 32, 66, 14);
		contentPane.add(lblNrClienti);
		
		textField_6 = new JTextField();
		textField_6.setBounds(407, 44, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		//starts the simulation
		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Queue> cozi = new ArrayList<Queue>();
				List<Thread> threads = new ArrayList<Thread>();
				Random rand = new Random();
				int maxNrOfCli=0;
				long peakHour=0;
				int auxNrOfCli;
				int servBeg; 
				int servEnd; 
				int nrOfQueues;
				int arrivTimeBetwCliInf;
				int arrivTimeBetwCliSup;
				int clientTimeWait=0;
				int serveClientTimeInf;
				int serveClientTimeSup;
				Queue coadaAux;
				nrOfQueues=Integer.parseInt(textField_6.getText());
				servBeg=Integer.parseInt(textField.getText());
				servEnd=Integer.parseInt(textField_1.getText());
				serveClientTimeInf=Integer.parseInt(textField_2.getText());
				serveClientTimeSup=Integer.parseInt(textField_3.getText());
				arrivTimeBetwCliInf=Integer.parseInt(textField_4.getText());
				arrivTimeBetwCliSup=Integer.parseInt(textField_5.getText());
				//lists creation
				for(int i=0;i<nrOfQueues;i++){
					coadaAux=new Queue(i);
					cozi.add(coadaAux);
				}
				//thread initialization
				for(int i=0;i<nrOfQueues;i++){
					
					threads.add(new Thread(cozi.get(i)));
				}
				long startTime= System.currentTimeMillis();
				int queueIndex=0;
				txtrJnnj.setCaretPosition(txtrJnnj.getDocument().getLength());
				while(System.currentTimeMillis()-startTime<((servEnd-servBeg)*1000)){
					clientTimeWait=rand.nextInt(serveClientTimeSup-serveClientTimeInf) + 1 + serveClientTimeInf; 
					Client c = new Client(clientTimeWait);
					queueIndex=getFastestQueue(cozi);
					cozi.get(queueIndex).addClient(c);
					auxNrOfCli=getPeakHour(cozi);
					if(auxNrOfCli > maxNrOfCli){
						maxNrOfCli=auxNrOfCli;
						peakHour=System.currentTimeMillis();
					}	
					txtrJnnj.append("ADDED:Client added to queue nr. " + queueIndex + "\n");
					txtrJnnj.update(txtrJnnj.getGraphics());
					txtrJnnj.setCaretPosition(txtrJnnj.getDocument().getLength());
					try{
						Thread.sleep(rand.nextInt(arrivTimeBetwCliSup-arrivTimeBetwCliInf) + 1 + arrivTimeBetwCliInf);
					}catch(Exception e){
						
					}
				}
				
				try{
					Thread.sleep(clientTimeWait);
					}catch(Exception e){
						
					}
				for(int i=0;i<nrOfQueues;i++){
					
					threads.add(new Thread(cozi.get(i)));
				}
				for(int i=0;i<nrOfQueues;i++){
					
					threads.get(i).start();
				}
				try{
				threads.get(queueIndex).join();
				}catch(Exception e){
					
				}
				//the peak time 
				txtrJnnj.append(Queue.getText());
				txtrJnnj.append("Time at peak: " + ((peakHour/(1000*3600))%24 < 10 ? ("0" +(peakHour/(1000*3600))%24) : (peakHour/(1000*3600))%24) + ":" + (peakHour/(1000*60) % 60<10 ? "0" + peakHour/(1000*60) % 60 : peakHour/(1000*60) % 60) + ":" + ((peakHour/(1000)%60)<10 ? "0" + (peakHour/(1000)%60) : (peakHour/(1000)%60)));
				//average serving time of each queue
				for(int i=0;i<cozi.size();i++){
					txtrJnnj.update(txtrJnnj.getGraphics());
					txtrJnnj.setCaretPosition(txtrJnnj.getDocument().getLength());
					txtrJnnj.append("\nThe average serving time for the queue nr. "+ cozi.get(i).getQueueNr()+ " is " +(cozi.get(i).getPassingTime()/cozi.get(i).getClientNr())+" ms.");
				}
			}
		});
		btnStart.setBounds(407, 71, 89, 23);
		contentPane.add(btnStart);
		
		JButton btnClear = new JButton("CLEAR");
		//clears the log
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtrJnnj.setText("");
			}
		});
		btnClear.setBounds(407, 105, 89, 23);
		contentPane.add(btnClear);
	}
	//returns the index of the queue with the shortest waiting time
	public static int getFastestQueue(List<Queue> coada){
		Iterator<Queue> it=coada.iterator();
		int min=coada.get(0).getWaitingTime();
		int i=0,iFast=0;
		Queue q;
		while(it.hasNext()){
			q=it.next();
			if(q.getWaitingTime()<min){
				min=q.getWaitingTime();
				iFast=i;
			}
			i++;
		}
		
		return iFast;
	}
	//returns the number of clients at the queues
	public static int getPeakHour(List<Queue> cozi){
		Iterator<Queue> it = cozi.iterator();
		Queue aux;
		int cliNr=0;
		while(it.hasNext()){
			aux=it.next();
			cliNr=cliNr+aux.getQueue().size();
		}
		return cliNr;
	}
}
